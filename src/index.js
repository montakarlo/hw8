import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {createStore, applyMiddleware, compose} from 'redux'
import {Provider} from 'react-redux'
import reduxThunk from 'redux-thunk'
import rootReducer from './redux/rootReducer'
import createSagaMiddleware from 'redux-saga'
import { sagaWatcher } from './redux/sagas';
import {Router} from 'react-router-dom'
import { createBrowserHistory } from "history";

const history = createBrowserHistory();
const saga = createSagaMiddleware()

const store = createStore(rootReducer, compose(
  applyMiddleware(
  reduxThunk, saga
)))
// const store = createStore(rootReducer)

saga.run(sagaWatcher)

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
    <Router history={history}>
        <App />
      </Router>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

serviceWorker.unregister();

