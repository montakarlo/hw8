import React, { Component } from 'react';
import Chat from './components/Chat/Chat'
import LogIn from './components/LogIn/LogIn'
import EditMessage from './components/EditMessage/EditMessage'
import UserList from './components/UserList/UserList'
import UserEditor from './components/UserEditor/UserEditor'
import './App.sass'
import {NavLink, Route, Switch, Redirect} from 'react-router-dom'

class App extends Component {

  render(){
    return (
      <div className="pageContainer">
        {/* <Switch> */}
          <Route path="/" exact component={LogIn}/>
          <Route path="/chat" component={Chat}/>
          <Route path="/editMessage" component={EditMessage}/>
          <Route path="/userList" component={UserList}/>
          <Route path="/userEditor" component={UserEditor}/>
          {/* <Redirect from="*" to="/" /> */}
        {/* </Switch> */}
      </div>
    )
  }
}

export default App;
