import {LOG_IN,
  SHOW_LOADER,
  HIDE_LOADER,
  FETCH_MESSAGES} from '../actions/actionTypes';

const initialState = {
  logInAnswer: null
}

export default function messages (state = initialState, action) {

  switch(action.type) {
    case LOG_IN:
      return {
        logInAnswer: action.payload
      }
    default:
      return state
  }
}
