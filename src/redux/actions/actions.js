import {FETCH_MESSAGES,
        ADD_MESSAGE,
        DELETE_MESSAGE,
        LIKE_MESSAGE,
        TAKE_ALL_MESSAGES,
        EDIT_MESSAGE,
        LOG_IN,
        SHOW_LOADER,
        HIDE_LOADER} from './actionTypes'

export function fetchedToStore(link) {
  return (dispatch) => {
    dispatch(fetchMessages(link))
  }
}

export function fetchMessages(link) {
  return {
    type: FETCH_MESSAGES,
    payload: link
  }
}

export function addMessage(newMessage) {
  return {
    type: ADD_MESSAGE,
    payload: newMessage
  }
}

export function deleteMessage(id) {
  return {
    type: DELETE_MESSAGE,
    payload: id
  }
}

export function likeMessage(id) {
  return {
    type: LIKE_MESSAGE,
    payload: id
  }
}

export function takeAllMessages(){
  return {
    type: TAKE_ALL_MESSAGES
  }
}

export function editMessage(id, text){
  return {
    type: EDIT_MESSAGE,
    payload: {id, text}
  }
}

export function logIn(logInObj){
  return async dispatch => {
    dispatch(showLoader())
    const response = await fetch('http://localhost:3050/api/auth',{
      method: 'POST',
      headers: {'Content-Type': 'application/json;charset=utf-8'},
      body: JSON.stringify(logInObj)
    });
    const json = await response.json();
    setTimeout(() => {
      dispatch({type: LOG_IN, payload: json});
      dispatch(hideLoader())
    }, 500);
  }
}

export function showLoader() {
  return {
    type: SHOW_LOADER
  }
}

export function hideLoader() {
  return {
    type: HIDE_LOADER
  }
}