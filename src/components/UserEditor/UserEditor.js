import React, {Component} from 'react';
import './UserEditor.sass'
import Pageheader from '../Chat/Pageheader/Pageheader'
import {connect} from 'react-redux'
import {NavLink, Route, Switch, Redirect} from 'react-router-dom'


export class UserEditor extends Component {

  // componentDidMount = () => {
  //   this.props.onFetch('https://edikdolynskyi.github.io/react_sources/messages.json');
  // }
  goToUserList = () => {
    this.props.history.push({
      pathname: '/userList'
    })
  }
  
  render(){
    return(
      <div className = "container">
        <Pageheader text='User editor' className="pageHeader"/>
        <form action="submit" className='logInForm'>
          <div className='logInFormContainer'>

            <input
              type="text"
              id="input_userName"
              name="input_text"
              // value={this.state.message}
              // onChange={this.onChangeMessage}
              placeholder="User name"
              >
            </input>

            <input
              type="text"
              id="input_avatarLink"
              name="input_text"
              // value={this.state.message}
              // onChange={this.onChangeMessage}
              placeholder="Avatar link"
              >
            </input>

            <input
              type="text"
              id="input_password"
              name="input_text"
              // value={this.state.message}
              // onChange={this.onChangeMessage}
              placeholder="Password"
              >
            </input>

            <input
              type="text"
              id="input_passwordConfirm"
              name="input_text"
              // value={this.state.message}
              // onChange={this.onChangeMessage}
              placeholder="Confirm password"
              >
            </input>

            <button
              id = "submitButton"
              type="submit"
              value="Submit"
              className="logIn__button"
              onClick={this.goToUserList}
            >Ok
            </button>
          </div>
        </form>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    loaded: state.loaded
  }
}

// function mapDispatchToProps(dispatch) {
//   return {
//     onFetch: link => dispatch(fetchedToStore(link)),
//   }
// }

export default connect(mapStateToProps, null)(UserEditor)