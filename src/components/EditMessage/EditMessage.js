import React, {Component} from 'react';
import './EditMessage.sass'
import Pageheader from '../Chat/Pageheader/Pageheader'
import {connect} from 'react-redux'
import {NavLink, Route, Switch, Redirect} from 'react-router-dom'


export class EditMessage extends Component {

  // componentDidMount = () => {
  //   this.props.onFetch('https://edikdolynskyi.github.io/react_sources/messages.json');
  // }
  goToChat = () => {
    this.props.history.push({
      pathname: '/chat'
    })
  }

  render(){
    return(
      <div className = "container">
        <div className='pageHeaderContainer'>
          <Pageheader text='Edit message' className="pageHeader"/>
          {/* <NavLink  to='/chat'>{`<Back to chat`}</NavLink> */}
        </div>
        <form className="editArea">
            <div className="editAreaContainer">
              <textarea
                className="textareaEditArea"
                type="text"
                id="edit_message"
                name="edit_text"
                // value={this.state.message}
                // onChange={this.onChangeMessage}
                placeholder="Type message!"
                >
              </textarea>
            </div>
            <div className='editButtonContainer'>
              <button
                type="submit"
                value="Submit"
                className="editArea__button editArea__button_cancel"
                onClick={this.goToChat}
                // onClick={ () => {
                //   let toSend = Object.assign({},this.newMessageObj)
                //   this.newMessageObj.text = ''
                //   this.props.onAdd(toSend);
                //   this.clearArea();
                // }}
                >Cancel
              </button>
              <button
                type="submit"
                value="Submit"
                className="editArea__button"
                onClick={this.goToChat}
                // onClick={ () => {
                //   let toSend = Object.assign({},this.newMessageObj)
                //   this.newMessageObj.text = ''
                //   this.props.onAdd(toSend);
                //   this.clearArea();
                // }}
                >Ok
              </button>
            </div>
        </form>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    loaded: state.loaded
  }
}

// function mapDispatchToProps(dispatch) {
//   return {
//     onFetch: link => dispatch(fetchedToStore(link)),
//   }
// }

export default connect(mapStateToProps, null)(EditMessage)
