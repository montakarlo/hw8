import React, {Component} from 'react';
import './LogIn.sass'
import Pageheader from '../Chat/Pageheader/Pageheader'
import {connect} from 'react-redux'
import {NavLink, Route, Switch, Redirect} from 'react-router-dom'
import Chat from '../Chat/Chat'
import {BrowserRouter} from 'react-router-dom'
import {logIn} from '../../redux/actions/actions'
import {Loader} from '../Loader/Loader'


export class LogIn extends Component {

    constructor(props){
      super(props);
      this.state = {
        user: '',
        password: ''
      }
      this.onSubmit = this.onSubmit.bind(this);
      this.onChange = this.onChange.bind(this);
    }

  goToChat = () => {
    this.props.history.push({
      pathname: '/chat'
    })
  }

  goToUserList = () => {
    this.props.history.push({
      pathname: '/userList'
    })
  }
  route = () => {
    let answer = this.props.logInAnswer.logInAnswer
    if (answer === 'user') {
      this.goToChat()
    } else if (answer === 'admin'){
      this.goToUserList()
    }
  }

  async onSubmit(e) {
      e.preventDefault();
      await this.props.onLogIn(this.state)
      this.setState({user: '', password: ''})
  }

  onChange(e) {
    this.setState({[e.target.name]: e.target.value});
  }
  clearArea(){
    document.getElementById('input_userName').value = "";
    document.getElementById('input_password').value = "";
  }
  render(){
    if (this.props.loading){
      return <Loader />
    }
    this.route()
    return(
      <div className = "container">
        <Pageheader text='Log in' className="pageHeader"/>
        <form onSubmit={this.onSubmit} className='logInForm'>
          <div className='logInFormContainer'>

            <input
              type="text"
              id="input_userName"
              name="user"
              // value={this.state.message}
              onChange={this.onChange}
              placeholder="User name"
              >
            </input>

            <input
              type="password"
              id="input_password"
              name="password"
              // value={this.state.message}
              onChange={this.onChange}
              placeholder="Password"
              >
            </input>

            <button
              id = "submitButton"
              type="submit"
              value="Submit"
              className="logIn__button"
              onClick={this.clearArea}
              // onClick={() => {this.props.onLogIn(this.state)}}
              
            >Log In
            </button>
          </div>
        </form>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    logInAnswer: state.logIn,
    loading: state.appReducer.loading
  }
}

function mapDispatchToProps(dispatch) {
  return {
    onLogIn: body => dispatch(logIn(body)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LogIn)