import React, {Component} from 'react';
import './UserList.sass'
import Pageheader from '../Chat/Pageheader/Pageheader'
import {connect} from 'react-redux'
import {NavLink, Route, Switch, Redirect} from 'react-router-dom'
import UserItem from './UserItem/UserItem'

export class UserList extends Component {

  // componentDidMount = () => {
  //   this.props.onFetch('https://edikdolynskyi.github.io/react_sources/messages.json');
  // }
  goToUserEditor = () => {
    this.props.history.push({
      pathname: '/userEditor'
    })
  }
  render(){
    return(
      <div className = "container">
        <div className='pageHeaderContainer'>
          <Pageheader text='User list' className="pageHeader"/>
          <NavLink  to='/chat'>{`<Go to chat`}</NavLink>
        </div>
        <div className='usersContainer'>
          <button
            id = "submitButton"
            type="submit"
            value="Submit"
            className="addUser__button"
            onClick={this.goToUserEditor}
          >Add user
          </button>
          <div className='usersArea'>
            <div className='usersArea__container'>
              <UserItem history={this.props.history}/>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    loaded: state.loaded
  }
}

// function mapDispatchToProps(dispatch) {
//   return {
//     onFetch: link => dispatch(fetchedToStore(link)),
//   }
// }

export default connect(mapStateToProps, null)(UserList)