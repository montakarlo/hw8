import React, {Component} from 'react';
import './UserItem.sass'
import {connect} from 'react-redux'
import {NavLink, Route, Switch, Redirect} from 'react-router-dom'

export class UserItem extends Component {
  constructor(props){
    super(props);
  }
  // componentDidMount = () => {
  //   this.props.onFetch('https://edikdolynskyi.github.io/react_sources/messages.json');
  // }

  // goToChat = () => {
  //   this.props.history.push({
  //     pathname: '/chat'
  //   })
  // }
  goToUserEditor = () => {
    this.props.history.push({
      pathname: '/userEditor'
    })
  }
  render(){
    return(
      <div className='userItemContainer'>
        <div className='userName'>
          <span>User1</span>
        </div>
        <div className='userButtonsContainer'>
          <button
            id = "submitButton"
            type="submit"
            value="Submit"
            className="user__button deleteUser__button"
            // onClick={this.goToChat}
          >Delete
          </button>
          <button
            id = "submitButton"
            type="submit"
            value="Submit"
            className="user__button editUser__button"
            onClick={this.goToUserEditor}
          >Edit
          </button>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    loaded: state.loaded
  }
}

// function mapDispatchToProps(dispatch) {
//   return {
//     onFetch: link => dispatch(fetchedToStore(link)),
//   }
// }

export default connect(mapStateToProps, null)(UserItem)