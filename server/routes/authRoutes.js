const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { loginUserValid} = require('../middlewares/auth.validation.middleware');

const router = Router();

// router.get("/", (req, res) =>{
//   res.status(200);
//   res.send(AuthService.getAll());

// })
router.post("/", loginUserValid, responseMiddleware, (req,res) => {
  let response = req.body[1]
  res.status(200);
  res.json(response);
});

module.exports = router;