const { BaseRepository } = require('./baseRepository');

class AuthRepository extends BaseRepository {
    constructor() {
        super('users');
    }
}

exports.AuthRepository = new AuthRepository();