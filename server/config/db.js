const path = require('path');
const dbPath = `${path.resolve()}/database.json`;
const { messages } = require('./messages');
const { users } = require('./users');

const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
const adapter = new FileSync(dbPath);

const dbAdapter = low(adapter);

const defaultDb = { users, messages};

dbAdapter.defaults(defaultDb).write();

exports.dbAdapter = dbAdapter;