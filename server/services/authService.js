const { AuthRepository } = require('../repositories/authRepository');

class AuthService {
  search(search) {
    const item = AuthRepository.getOne(search);
    if(!item) {
        return false;
    }
    return item;
  }
  allUsers(){
    const items = AuthRepository.getAll();
    if(!items.length) {
        return [];
    }
    return items;
}
}

module.exports = new AuthService();