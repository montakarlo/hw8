const { login } = require('../models/login');
const AuthService = require('../services/authService');

let ValidateFields = (ArrToValidate, model) => {
    let exist = true
    model.forEach(element => {
        if (exist && !ArrToValidate.includes(element)){
            exist = false;
            return exist
        }
    });
    return exist
}

let deleteFromArr = (arr, value) => {
    return arr.filter(item => item !== value)
}

let deleteExternalFields = (objToDel, model) =>{
    let objToDelKeys = Object.keys(objToDel);
    let modelKeys = Object.keys(model);
    objToDelKeys.forEach(element => {
        !modelKeys.includes(element) ? delete objToDel[`${element}`] : 0;
    });
    return objToDel
}

let checkForSameKeyValue = (ObjToCheck, key, base) =>{
    let answer = false;
    base.forEach(element => {
        element[key] == ObjToCheck[key] && !answer ? answer = true : false;
    });
    return answer
}

let checkForSameId = (key, value, base) =>{
    let answer = false;
    base.forEach(element => {
        String(element[key]) == String(value) && !answer ? answer = true : false;
    });
    return answer
}

let getObjectByKey = (key, value, base) =>{
    let userObj = {}
    base.forEach(element => {
        element[key] == value ? userObj = {...element} : 0;
    });
    return userObj
}

let checkUserAndPassword = (ObjToCheck, base) => {
  let answer = false;
  base.forEach(element => {
      element['user'] == ObjToCheck['user'] && element['password'] == ObjToCheck['password'] && !answer ? answer = true : false;
  });
  return answer
}

const loginUserValid = (req, res, next) => {
    console.log(req.body);
    let inputObj = req.body;
    let user = inputObj.user
    let password = inputObj.password
    let loginKeys = Object.keys(login);
    loginKeys = deleteFromArr(loginKeys, 'userId');
    let inputObjKeys = Object.keys(inputObj);
    let base = AuthService.allUsers();
    if (!Object.keys(inputObj).length){
        req.body = [400, 'Request with empty data'];
        next();
    } else if (!ValidateFields(inputObjKeys, loginKeys)){
        req.body = [400, 'Missing some fields'];
        next();
    } else if (inputObj['user'] === 'admin' && inputObj['password'] === 'admin') {
        req.body = [200, 'admin'];
        next();
    } else if (inputObj['password'].length<6){
        req.body = [400, 'Password must contain more than 6 symbols'];
        next();
    } else if (!checkUserAndPassword(inputObj, base)){
        req.body = [400, 'Incorrect password or username'];
        next();
    } else {
        // req.body = deleteExternalFields(inputObj, login)
        req.body = [200, 'user'];
        next();
    }
}


exports.loginUserValid = loginUserValid;

